$(function(){

    // set scroll animation
    var offsetY = -80;
    var time = 1200;

    $("a[href^='#']").click(function(e){

        // デフォルトの処理をキャンセル
        e.preventDefault();

        // 移動先となる要素を取得
        var target = $(this.hash);
        if (!target.length) return ;

        // 移動先となる値
        var targetY = target.offset().top+offsetY;

        // スクロールアニメーション
        $('html,body').animate({scrollTop: targetY}, time, 'swing');
        // ハッシュ書き換えとく
        window.history.pushState(null, null, this.hash);
    });

    // set menu action
    // 320px - 768px
    $(".menu_img,.close_img").click(function(){
        $(this).toggleClass("vis");
        if($(this).hasClass("menu_img")){
            $(".close_img").toggleClass('vis');
            $(".navi").show();
        }
        else{
            $(".menu_img").toggleClass('vis');
            $(".navi").hide();
        }
    });
    //
    $(".navi a").click(function(){
        $(".close_img").toggleClass('vis');
        $(".menu_img").toggleClass('vis');
        $(".navi").hide();
    });

    $(".fv_carousel").carousel({
        interval: 3000
    });

    // if($(window).width() <= 360){
    //   $(".modal-image-wrap").attr("data-target",".bs-example-modal-sm");
    //   $(".modal").removeClass(".bs-example-modal-lg").addClass(".bs-example-modal-sm");
    // }

    $(".modal-image-wrap").click(function(){

      var src = $(this).find("img").attr("src");
      $(".modal-image").attr("src",src);
    });
    //
    // // Layout Other size image
    // $(".image_list .item_wrap").wookmark({
    //   fillEmptySpace:true
    // });
});
