<?php
    mb_language("Japanese");
    mb_internal_encoding("UTF-8");

    $company_name = "会社名: ".$_POST['company_name']."\r\n";
    $person_name  = "担当者名: ".$_POST['person_name']."\r\n";
    $name_ruby    = "担当者フリガナ: ".$_POST['name_ruby']."\r\n";
    $phone_number = "電話番号: ".$_POST['phone_number']."\r\n";
    $mail         = "メールアドレス: ".$_POST['mail']."\r\n";
    $place        = "希望の場所: ".$_POST['place']."\r\n";
    $use          = "使用用途: ".$_POST['use']."\r\n";
    $content      = "撮影内容: ".$_POST['content']."\r\n";
    $start_time   = "開始時間: ".$_POST['s_hour']." : ".$_POST['s_minute']."\r\n";
    $finish_time  = "終了時間: ".$_POST['f_hour']." : ".$_POST['f_minute']."\r\n";
    $other        = "備考: ".$_POST['other'];

    $email = $mail;
    $subject = "テストの件名"; // 題名
    $body = $company_name.
            $person_name.
            $name_ruby.
            $phone_number.
            $mail.
            $place.
            $use.
            $content.
            $start_time.
            $finish_time.
            $other; // 本文
    $to = 'yuuusuke0058@gmail.com';
    $header = "From: $email";

    mb_send_mail($to, $subject, $body, $header);

    echo '<pre>';
    print_r($_POST);
    echo '<pre>';
?>
